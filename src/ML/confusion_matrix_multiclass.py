import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
pd.options.display.max_columns = None

def plot_confusion_matrix_multiclass(confusion_matrix):
    df_cm = pd.DataFrame(confusion_matrix, index=['0','1','2','3'], columns=['0','1','2','3'])
    plt.figure(figsize=(10,10))
    sns.set(font_scale=1.5)
    #group_names = ['True Neg','False Pos','False Neg','True Pos']
    group_counts = ["{0:0.0f}".format(value) for value in confusion_matrix.flatten()]
    group_percentages = ["{0:.2%}".format(value) for value in confusion_matrix.flatten()/np.sum(confusion_matrix)]
    labels = ['\n'.join(sorted({v1,v2}, key=len)) for v1, v2 in zip(group_counts,group_percentages)]
    labels = np.asarray(labels).reshape(4,4)
    sns.heatmap(df_cm, annot=labels, fmt='', square=True)
    plt.xlabel('Predicted Class', fontsize=16)
    plt.ylabel('True Class', fontsize=16)