import pandas as pd
import matplotlib.pyplot as plt


def plot_Mutual_Info(mi, x_train):
    mi = pd.Series(mi)  # convert to series
    mi.index = x_train.columns  # columns as indexes
    mi.sort_values(ascending=False, inplace=True)
    # plot mutual information
    mi.plot.bar(figsize=(25, 10))
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=10)
    plt.xlabel("Feature Index", fontsize=16)
    plt.title("Mutual Information", fontsize=20)
