import pandas as pd
import requests
import io

urlData = requests.get(
    "https://raw.githubusercontent.com/SuperDataWorld/Python/main/loan.csv"
).content
rawData = pd.read_csv(io.StringIO(urlData.decode("utf-8")))
print(type(rawData))


# 1. Drop Rows with Null Columns
def drop_nulls(df, cols):
    df.dropna(subset=cols, inplace=True)
    return df


# 2. Fill Null Columns with Av Values
def fill_vals(df, cols):
    for i in cols:
        av = df[i].mean()
        df[i].fillna(av, inplace=True)
        return df


# 3. Replace Strings with numbers and convert type to numeric
def replace_strings(df, cols):
    for i in cols:
        df[i].replace("3+", 4, inplace=True)
        df[i] = pd.to_numeric(df[i])
        return df


null_cols = [
    "Gender",
    "Married",
    "Dependents",
    "Credit_History",
    "Self_Employed",
    "Loan_Amount_Term",
]
av_cols = ["LoanAmount"]
rp_cols = ["Dependents"]

df = (
    rawData.pipe(drop_nulls, null_cols)
    .pipe(fill_vals, av_cols)
    .pipe(replace_strings, rp_cols)
)

print(df.info())
