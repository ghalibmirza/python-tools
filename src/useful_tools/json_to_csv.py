import json
import csv
import os


def json_to_csv(input_dir, output_dir):
    # Create the output directory if it doesn't exist
    if not os.path.exists(output_dir):
        print("Directory does not exist, creating directory")
        os.makedirs(output_dir)

    # Get a list of all JSON files in the input directory
    json_files = [f for f in os.listdir(input_dir) if f.endswith(".json")]

    # Iterate over each JSON file
    for json_file in json_files:
        # Open the JSON file
        with open(os.path.join(input_dir, json_file), encoding="UTF-8") as f:
            data = json.load(f)

        # Create the output CSV file name
        csv_file = os.path.splitext(json_file)[0] + ".csv"

        # Get the headers from the JSON data
        headers = list(data[0].keys())

        # Create the output CSV file
        with open(os.path.join(output_dir, csv_file), "w", newline="") as f:
            writer = csv.DictWriter(f, fieldnames=headers)

            # Write the headers to the CSV file
            writer.writeheader()

            # Write each JSON object as a row in the CSV file
            writer.writerows(data)


input_directory = "./data/json/"
output_directory = "./data/csv/"

json_to_csv(input_directory, output_directory)
