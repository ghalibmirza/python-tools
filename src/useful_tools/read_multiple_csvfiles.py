import pandas as pd
import glob

def append_csv_files(folder_path, file_extension='.csv'):
    master_df = pd.DataFrame()

    # Get a list of all files with the given extension in the folder
    file_paths = glob.glob(folder_path + '/*' + file_extension)

    # Iterate over each file path
    for file_path in file_paths:
        # Read the file in chunks
        for chunk in pd.read_csv(file_path, chunksize=10000):
            # Append the chunk to the combined data
            master_df = pd.concat([master_df, chunk])
    return master_df