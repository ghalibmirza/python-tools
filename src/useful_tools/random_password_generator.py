import random
import string


def pass_generator(pass_length: int, avoid_chars: str = ""):
    total_char = string.ascii_letters + string.digits + string.punctuation

    # Remove the characters to avoid from the total_char string
    total_char = "".join(char for char in total_char if char not in avoid_chars)

    length = pass_length
    password = "".join(random.sample(total_char, length))
    return password


password = pass_generator(16, "~.><,:;")
print(password)
