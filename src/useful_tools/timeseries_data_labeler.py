import os
import csv
from dateutil.parser import isoparse
# import datatable as dt

def label_csv_rows_single_event_binary(start_time, end_time, csv_file, output_dir):
    start_time = isoparse(start_time)
    end_time = isoparse(end_time)
    new_column_name = 'manual_tag_state'

    with open(csv_file, 'r') as file:
        reader = csv.DictReader(file)
        fieldnames = reader.fieldnames + [new_column_name]
        rows = list(reader)

    for row in rows:
        row_start_time = isoparse(row['_time'])
        row_end_time = isoparse(row['_time'])

        if start_time <= row_start_time <= end_time or start_time <= row_end_time <= end_time:
            row[new_column_name] = 1
        else:
            row[new_column_name] = 0

    filename = os.path.basename(csv_file)
    output_file = os.path.join(output_dir, f'labeled_{filename}')

    os.makedirs(output_dir, exist_ok=True)  # Create the output directory if it doesn't exist

    with open(output_file, 'w', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(rows)

    print(f"Modified CSV file saved at: {output_file}")


def label_csv_rows_multi_event_binary(start_times, end_times, csv_file, output_dir):
    new_column_name = 'manual_tag_state'

    with open(csv_file, 'r') as file:
        reader = csv.DictReader(file)
        fieldnames = reader.fieldnames + [new_column_name]
        rows = list(reader)

    for row in rows:
        row_start_time = isoparse(row['_time'])
        row_end_time = isoparse(row['_time'])

        is_labeled = False
        for start_time, end_time in zip(start_times, end_times):
            start_time = isoparse(start_time)
            end_time = isoparse(end_time)
            if start_time <= row_start_time <= end_time or start_time <= row_end_time <= end_time:
                is_labeled = True
                break

        row[new_column_name] = 1 if is_labeled else 0

    filename = os.path.basename(csv_file)
    output_file = os.path.join(output_dir, f'labeled_{filename}')

    os.makedirs(output_dir, exist_ok=True)  # Create the output directory if it doesn't exist

    with open(output_file, 'w', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(rows)

    print(f"Modified CSV file saved at: {output_file}")


def label_csv_rows_multi_event_custom_labels(start_times, end_times, labels, csv_file, output_dir, default_label=0):
    new_column_name = 'manual_tag_state'

    with open(csv_file, 'r') as file:
        reader = csv.DictReader(file)
        fieldnames = reader.fieldnames + [new_column_name]
        rows = list(reader)

    for row in rows:
        row_start_time = isoparse(row['_time'])
        row_end_time = isoparse(row['_time'])

        label = default_label
        for start_time, end_time, lbl in zip(start_times, end_times, labels):
            start_time = isoparse(start_time)
            end_time = isoparse(end_time)
            if start_time <= row_start_time <= end_time or start_time <= row_end_time <= end_time:
                label = lbl
                break

        row[new_column_name] = label

    filename = os.path.basename(csv_file)
    output_file = os.path.join(output_dir, f'labeled_{filename}')

    os.makedirs(output_dir, exist_ok=True)  # Create the output directory if it doesn't exist

    with open(output_file, 'w', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(rows)

    print(f"Modified CSV file saved at: {output_file}")




# # for single event static (binary) labels
# start_time = '2023-05-31T00:00:11Z'
# end_time = '2023-05-31T00:00:21Z'
# csv_file = '../Data/0_Raw/test.csv'
# output_dir = '../Data/Labeled/'
# label_csv_rows_single_event_binary(start_time, end_time, csv_file, output_dir)

######################

# # For multi event static (Binary) labels
# start_times = ['2023-05-31T00:00:11Z', '2023-05-31T00:00:35Z']
# end_times = ['2023-05-31T00:00:21Z', '2023-05-31T00:00:55Z']
# csv_file = '../Data/0_Raw/test.csv'
# output_dir = '../Data/Labeled/'
# label_csv_rows_multi_event_binary(start_times, end_times, csv_file, output_dir)

#######################

# For multi event static (Binary) labels
start_times = ['2023-06-01T11:55:23Z', '2023-06-01T12:02:35Z']
end_times = ['2023-06-01T12:02:35Z', '2023-06-01T14:00:00Z']
csv_file = '../Data/0_Raw/test.csv'
output_dir = '../Data/Labeled/'
labels = [1, 2]
label_csv_rows_multi_event_custom_labels(start_times, end_times, labels, csv_file, output_dir)
