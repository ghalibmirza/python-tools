import csv
import os
import argparse
from datetime import datetime, timezone
from influxdb_client import InfluxDBClient

def influxdb_connector(url, token, org):
  client = InfluxDBClient(url=url, token=token, org=org)
  return client

def data_fetcher(url, token, org, bucket, topic, measurement, start_time, end_time):
  
  query = f'from(bucket: "{bucket}")\
          |> range(start: {start_time.isoformat()}, stop: {end_time.isoformat()})\
          |> filter(fn: (r) => r._measurement == "{measurement}")\
          |> filter(fn: (r) => r._field == "field1" or r["_field"] == "field2" or r["_field"] == "field3" or r["_field"] == "field4")\
          |> filter(fn: (r) => r["topic"] == "{topic}")\
          |> aggregateWindow(every: 1s, fn: last, createEmpty: false)\
          |> yield(name: "last")'
  
  # Connection with the DB
  connector_client = influxdb_connector(url, token, org)

  # Execute the query and retrieve the result as CSV
  csv_result = connector_client.query_api().query_csv(query)
  print("Data Fetched Successfully")
  return csv_result

def save_to_csv(csv_result, output_directory, filename):
  # Create the directory if it doesn't exist
  if not os.path.exists(output_directory):
      os.makedirs(output_directory)

  csv_filepath = os.path.join(output_directory, filename)

  with open(csv_filepath, 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)

    # Write data to the file
    for line in csv_result:
        writer.writerow(line)

  print(f"Data saved to {csv_filepath}.")


# Parse command-line arguments
parser = argparse.ArgumentParser(description='InfluxDB Data Exporter')
parser.add_argument('--url', required=True, type=str, help='InfluxDB URL')
parser.add_argument('--token', required=True, type=str, help='InfluxDB Token')
parser.add_argument('--org', required=True, type=str, help='InfluxDB Organization')
parser.add_argument('--bucket', required=True, type=str, help='InfluxDB Bucket')
parser.add_argument('--topic', required=True, type=str, help='Topic')
parser.add_argument('--fields', required=True, nargs='+', help='Fields')
parser.add_argument('--measurement', required=True, type=str, help='Measurement')
parser.add_argument('--start-time', required=True, type=str, help='Start Time (UTC)')
parser.add_argument('--end-time', required=True, type=str, help='End Time (UTC)')
parser.add_argument('--output-directory', required=True, type=str, help='Output Directory')
parser.add_argument('--output-filename', required=True, type=str, help='Output Filename')
args = parser.parse_args()

# Convert start_time and end_time to datetime objects
start_time = datetime.fromisoformat(args.start_time).replace(tzinfo=timezone.utc)
end_time = datetime.fromisoformat(args.end_time).replace(tzinfo=timezone.utc)

# Query InfluxDB
csv_result = data_fetcher(args.url, args.token, args.org, args.bucket, args.topic,
                        args.measurement, start_time, end_time)

# Save data to CSV file
output_directory = args.output_directory
output_filename = args.output_filename
save_to_csv(csv_result, output_directory, output_filename)

## output directory = './data/'
