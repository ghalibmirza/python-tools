import time

#first install azure-iot-device client using "sudo pip install azure-iot-device" 
from azure.iot.device import IoTHubDeviceClient

RECEIVED_MESSAGES = 0

#Device connection String
CONNECTION_STRING = "HostName=Lisios-IoT-Hub-Test.azure-devices.net;DeviceId=lisios_rpi_sim_000001;SharedAccessKey=vCjZEF3fa3YlMWSBCmHr896lFGyQErtLYzH5p97TxkE="

#function for printing messages to the console
def message_handler(message):
    global RECEIVED_MESSAGES
    RECEIVED_MESSAGES += 1
    print("")
    print("Message received:")

    # print data from both system and application (custom) properties
    #for property in vars(message).items():
        #print ("    {}".format(property))

    print("Total calls received: {}".format(RECEIVED_MESSAGES))


#main function to initialize the client and wait to receive the cloud-to-device messages
def main():
    print ("Starting the Python IoT Hub C2D Messaging device sample...")

    # Instantiate the client
    client = IoTHubDeviceClient.create_from_connection_string(CONNECTION_STRING)

    print ("Waiting for C2D messages, press Ctrl-C to exit")
    try:
        # Attach the handler to the client
        client.on_message_received = message_handler

        while True:
            time.sleep(1000)
    except KeyboardInterrupt:
        print("IoT Hub C2D Messaging device sample stopped")
    finally:
        # Graceful exit
        print("Shutting down IoT Hub Client")
        client.shutdown()


if __name__ == '__main__':
    main()
